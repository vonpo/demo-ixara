(function (Draggable, Slider) {
    var MAX_VALUE = 60;
    var draggableElement = document.getElementById('dragMe');
    var draggableSlider = document.getElementById('dragMeSlider');
    var draggable = new Draggable(draggableElement);
    var slider = new Slider(draggableSlider, {
        step: 1,
        maxValue: 60, // get half of our drag element 100px + 20px border / 2 = 60px;
        minValue: 0
    });

    slider.setValue(MAX_VALUE);
    var unsubscribe = slider.onPositionChange(function (data) {
        draggableElement.style.borderRadius = MAX_VALUE - data.value + 'px';
    })



}(window.demo.components.Draggable, window.demo.components.Slider));