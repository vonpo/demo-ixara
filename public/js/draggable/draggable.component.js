(function (root, throttle) {
    /**
     * Draggable handle constructor.
     * @param element DOM element which will be dragged.
     * @constructor
     */
    function Draggable(element) {
        var offsetX = 0;
        var offsetY = 0;

        var onMouseMove = function (e) {
            var event = e.detail;

            if (e.detail.touches) {
                event = e.detail.touches[0];
            }

            var left = event.clientX;
            var top = event.clientY;
            element.style.left = left - offsetX + 'px';
            element.style.top = top - offsetY + 'px';
        }.bind(this);

        var onMouseUp = function () {
            document.removeEventListener('mouseup', onMouseUp);
            document.removeEventListener('mouse_move_lazy', onMouseMove);
            this.unsubscribeMouseMoveFn ? this.unsubscribeMouseMoveFn() : null;
            this.unsubscribeTouchFn ? this.unsubscribeTouchFn() : null;

        }.bind(this);

        function onTouchEnd(e) {
            if (!e || !e.touches) {
                return;
            }
            if (e.touches.length === 0) {
                onMouseUp(e.changedTouches[0]);
            }
        }

        var onMouseDown = function (e) {
            var rect = element.getBoundingClientRect();

            if (e.touches) {
                offsetX = e.touches[0].clientX - rect.left;
                offsetY = e.touches[0].clientY - rect.top;
            } else {
                offsetX = e.clientX - rect.left;
                offsetY = e.clientY - rect.top;
            }

            this.unsubscribeMouseMoveFn = throttle('mousemove', 'mouse_move_lazy', {obj: document});
            this.unsubscribeTouchFn = throttle('touchmove', 'mouse_move_lazy', {obj: document});
            document.addEventListener('mouse_move_lazy', onMouseMove);
            document.addEventListener('mouseup', onMouseUp);
            document.addEventListener('touchend', onTouchEnd);
        }.bind(this);


        element.addEventListener('mousedown', onMouseDown);
        element.addEventListener('touchstart', onMouseDown);

        /**
         * Clean up.
         * Call this when component is removed from DOM.
         */
        this.destroy = function () {
            element.removeEventListener('mousedown', onMouseDown);
            element.removeEventListener('touchstart', onMouseDown);
            onMouseUp();
        };
    }

    root.Draggable = Draggable;
})(window.demo.components, window.demo.helpers.throttle);