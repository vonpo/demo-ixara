(function (root) {
    function PubSub() {
        var events = {};

        /**
         * Registers callback for given event.
         * @param {String} event
         * @param {Function} callback
         * @returns {Function}
         */
        this.on = function (event, callback) {
            if (!Array.isArray(events[event])) {
                events[event] = [];
            }

            var index = events[event].push(callback) - 1;

            return function () {
                delete events[event][index]
            };
        };

        /**
         * Emit data to registered subscribers.
         * @param {String} event
         * @param {Object} [data]
         */
        this.emit = function (event, data) {
            var subscribers = events[event];

            if (!Array.isArray(subscribers)) {
                return;
            }

            subscribers.forEach(function (callback) {
                typeof (callback) === 'function' && callback(data)
            });
        }
    }

    root.PubSub = PubSub;
}(window.demo.patterns));