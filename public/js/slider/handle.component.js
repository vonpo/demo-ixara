(function (root, PubSub, throttle) {
    /**
     * Slider handle constructor.
     * @param parent DOM container for slider.
     * @param handle DOM element that is handle for slider.
     * @constructor
     */
    function SliderHandle(parent, handle) {
        var lastPosition = 0;
        var handlePubSub = new PubSub();

        // Event handling
        function onMouseMove(e) {
            var clientX = 0;

            if (e.detail) {
                clientX = e.detail.touches ? e.detail.touches[0].clientX : e.detail.clientX;
            }

            var parentBounds = parent.getBoundingClientRect();
            var newPosition = calculateNewPosition(clientX, parentBounds);

            if (newPosition !== lastPosition) {
                lastPosition = newPosition;
                handlePubSub.emit('positionChange', {
                    position: newPosition,
                    percentage: newPosition / parentBounds.width
                })
            }
        }

        var onMouseUp = function() {
            document.removeEventListener('touchend', onTouchEnd);
            document.removeEventListener('mouseup', onMouseUp);
            document.removeEventListener('mouse_move_lazy', onMouseMove);
            this.unsubscribeMouseMoveFn ? this.unsubscribeMouseMoveFn() : null;
            this.unsubscribeTouchFn ? this.unsubscribeTouchFn() : null;
        }.bind(this);

        var onMouseDown = function (event) {
            event.stopPropagation();

            document.addEventListener('touchend', onTouchEnd);
            document.addEventListener('mouseup', onMouseUp);
            this.unsubscribeMouseMoveFn = throttle('mousemove', 'mouse_move_lazy', {obj: document});
            this.unsubscribeTouchFn = throttle('touchmove', 'mouse_move_lazy', {obj: document});
            document.addEventListener('mouse_move_lazy', onMouseMove);

            return false;
        }.bind(this);

        function onTouchEnd(e) {
            if (!e || !e.touches) {
                return;
            }
            if (e.touches.length === 0) {
                onMouseUp(e.changedTouches[0]);
            }
        }

        function calculateNewPosition(mouseX, parentBounds) {
            var mousePosition = mouseX - parentBounds.left;

            if (mouseX < 0 || mousePosition < 0) {
                return 0;
            } else if (mousePosition > parentBounds.width) {
                return parentBounds.width
            } else {
                return mousePosition;
            }
        }

        handle.addEventListener('mousedown', onMouseDown);
        handle.addEventListener('touchstart', onMouseDown);

        /**
         * Attaches handlers which are triggered when position of handle is changed.
         * @param handler {Function}
         * @returns {Function}
         */
        this.onPositionChange = function (handler) {
            return handlePubSub.on('positionChange', handler);
        };

        /**
         * Remove all bindings.
         */
        this.destroy = function () {
            onMouseUp();
            handle.removeEventListener('mousedown', onMouseDown);
            handle.removeEventListener('touchstart', onMouseDown);
        };

        /**
         * Get position for step.
         * @param value {Number}
         * @param options {Object}
         * @returns {Number}
         */
        this.getStepPosition = function (value, options) {
            var stepCount = Math.round(options.maxValue / options.step);
            var parentRect = parent.getBoundingClientRect();
            var newPosition = parentRect.width / stepCount * value;

            return calculateNewPosition(parentRect.left + newPosition, parentRect)
        };
    }

    root.SliderHandle = SliderHandle;
}(window.demo.components, window.demo.patterns.PubSub, window.demo.helpers.throttle));