(function (root, PubSub, SliderHandle) {
    /**
     * Constructor of slider class.
     * @param element DOM element which wraps handle it must have element with 'handle' class inside.
     * @param options {Object} options for 'maxValue', 'minValue' and 'step'
     * @constructor
     */
    function Slider(element, options) {
        this._sliderPubSub = new PubSub();
        this._model = {percentage: 0, value: 0};
        this._options = options || {
                minValue: 0,
                maxValue: 100,
                step: 0
            };
        this._element = element;
        this._handleElement = element.getElementsByClassName('handle')[0];
        this._handle = new SliderHandle(element, this._handleElement);

        var recalculate = this._recalculatePosition.bind(this);
        var removeHandle = this._handle.onPositionChange(recalculate);
        var onResize = function () {
            this.setPercentage(this._model.percentage);
        }.bind(this);

        window.addEventListener('resize', onResize);

        this.destroy = function () {
            this._handle.destroy();
            window.removeEventListener('resize', onResize);
            removeHandle();
        }.bind(this);
    }

    /**
     * Draws and changes position of slider handle.
     * @param position {Number} position in pixels.
     * @private
     */
    Slider.prototype._draw = function (position) {
        var maxRight = this._element.offsetWidth - this._handleElement.offsetWidth;
        if (position >= maxRight) {
            position = this._element.offsetWidth - this._handleElement.offsetWidth;
        }

        this._handleElement.style.marginLeft = position + 'px';
    };

    /**
     * Set position to nearest step.
     * @param value {Number} value of the slider.
     * @returns {{value: number, position}}
     * @private
     */
    Slider.prototype._recalculatePositionForStep = function (value) {
        var step = Math.round(value / this._options.step);
        var valueByStep = step * this._options.step;

        return {
            value: valueByStep >= this._options.maxValue || value >= this._options.maxValue ? this._options.maxValue : valueByStep,
            position: this._handle.getStepPosition(step, this._options)
        }
    };

    /**
     * Handles change of slider handle.
     * @param data {Object}
     * @private
     */
    Slider.prototype._recalculatePosition = function (data) {
        var value = Math.round(this._options.maxValue * data.percentage);
        var position = data.position;
        var newModel = {
            percentage: data.percentage,
            value: value
        };

        if (this._options.step > 0) {
            var modelByStep = this._recalculatePositionForStep(value);

            newModel.value = modelByStep.value;
            position = modelByStep.position;
        }

        this._draw(position);
        this._model = newModel;
        this._sliderPubSub.emit('sliderPositionChange', newModel);
    };

    /**
     * Attaches handlers to 'sliderPositionChange' event.
     * And returns function to remove handler from subscribers array.
     * @param handler {Function}
     * @returns {Function}
     */
    Slider.prototype.onPositionChange = function (handler) {
        return this._sliderPubSub.on('sliderPositionChange', handler)
    };

    /**
     * Get percentage.
     * Returns values from 0..1
     * @returns {Number}
     */
    Slider.prototype.getPercentage = function () {
        return this._model.percentage;
    };

    /**
     * Set percentage.
     * Sets values from  0..1.
     * @param percentage {Number}
     */
    Slider.prototype.setPercentage = function (percentage) {
        var element = this._element.getBoundingClientRect();
        percentage = parseFloat(percentage);

        this._recalculatePosition({
            percentage: percentage,
            position: element.width * percentage
        })
    };

    /**
     * Get value of the slider.
     * @returns {Number}
     */
    Slider.prototype.getValue = function () {
        return this._model.value;
    };

    /**
     * Set value of the slider.
     * @param value {Number}
     */
    Slider.prototype.setValue = function (value) {
        this.setPercentage(parseInt(value) / this._options.maxValue)
    };


    root.Slider = Slider;
})(window.demo.components, window.demo.patterns.PubSub, window.demo.components.SliderHandle);