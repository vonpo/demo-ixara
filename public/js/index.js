(function (Slider) {
    var color = document.getElementById('color');
    var r = {
        sliderId: 'sliderR',
        percentageId: 'rColorPercentage',
        valueId: 'rColorAbsolute',
        step: 10,
        min: 0,
        max: 255
    };
    var g = {
        sliderId: 'sliderG',
        percentageId: 'gColorPercentage',
        valueId: 'gColorAbsolute',
        step: 20,
        min: 0,
        max: 255
    };
    var b = {
        sliderId: 'sliderB',
        percentageId: 'bColorPercentage',
        valueId: 'bColorAbsolute',
        step: 0,
        min: 0,
        max: 255
    };

    function rgb(r, g, b) {
        return ['rgb(', r, ',', g, ',', b, ')'].join('');
    }

    function updateColor() {
        color.style.backgroundColor = rgb(r.color, g.color, b.color);
    }

    color.style.backgroundColor =
        [r, g, b].forEach(function (sliderData) {
            var sliderElement = document.getElementById(sliderData.sliderId);
            var colorPercentage = document.getElementById(sliderData.percentageId);
            var colorAbsolute = document.getElementById(sliderData.valueId);
            var slider = new Slider(sliderElement, {
                minValue: sliderData.min,
                maxValue: sliderData.max,
                step: sliderData.step
            });

            colorPercentage.value = slider.getPercentage();
            colorAbsolute.value = slider.getValue();
            sliderData.color = colorAbsolute.value;

            slider.onPositionChange(function (data) {
                colorPercentage.value = data.percentage;
                colorAbsolute.value = data.value;
                sliderData.color = data.value;
                updateColor();
            });

            colorPercentage.addEventListener('change', function () {
                slider.setPercentage(colorPercentage.value);
            });

            colorAbsolute.addEventListener('change', function () {
                slider.setValue(colorAbsolute.value);
            });
        })
}(window.demo.components.Slider));