(function (root) {
    function polyfillCustomEvents() {
        // from https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
        if (typeof window.CustomEvent === "function") return false;

        function CustomEvent(event, params) {
            params = params || {bubbles: false, cancelable: false, detail: undefined};
            var evt = document.createEvent('CustomEvent');
            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return evt;
        }

        CustomEvent.prototype = window.Event.prototype;

        window.CustomEvent = CustomEvent;
    }

    /**
     * Based on
     * https://developer.mozilla.org/en-US/docs/Web/Events/resize.
     *
     * @param eventTarget {String} event to listen.
     * @param eventDestination {String} event to produce.
     * @param options {Object}
     * @returns {Function} Detach function.
     */
    function throttle(eventTarget, eventDestination, options) {
        options = options || {};

        var obj = options.obj || window;
        var running = false;

        function handle(e) {
            e.preventDefault();
            if (running) {
                return;
            }
            running = true;
            requestAnimationFrame(function () {
                obj.dispatchEvent(new CustomEvent(eventDestination, {detail: e}));
                running = false;
            });
        }

        obj.addEventListener(eventTarget, handle);

        return function () {
            obj.removeEventListener(eventTarget, handle);
        };
    }

    root.throttle = throttle;
    polyfillCustomEvents();
}(window.demo.helpers));