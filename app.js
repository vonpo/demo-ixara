const express = require('express');
const app = express();
const http = require('http').Server(app);

app.use('/xara', express.static(__dirname + '/public'));
app.all('/xara/*', function (req, res) {
    res.sendFile('index.html', {root: __dirname + '/public'});
});

var server = http.listen(5555, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.info('Demo info app listening at http://%s:%s', host, port);
});